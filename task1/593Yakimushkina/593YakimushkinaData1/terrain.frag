#version 330

in vec4 color;

out vec4 c;

void main()
{
    if( color.a <= 0.1 ) discard;
    c = color;
}
