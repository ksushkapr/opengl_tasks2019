#include "Mesh.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

#include <SOIL2.h>


MeshPtr makeSphere(float radius, unsigned int N)
{
    unsigned int M = N / 2;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (unsigned int i = 0; i < M; i++)
    {
        float theta = (float)glm::pi<float>() * i / M;
        float theta1 = (float)glm::pi<float>() * (i + 1) / M;

        for (unsigned int j = 0; j < N; j++)
        {
            float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
            float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

            //Первый треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

            //Второй треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeTree(float height, MeshPtr& leaves) {
	float radius = height / 3.;
	int N = 20;
	unsigned int M = 10;
	float diag = height / 30.;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;

	for (unsigned int i = 0; i < M; i++)
	{
		float theta = (float)glm::pi<float>() * i / M;
		float theta1 = (float)glm::pi<float>() * (i + 1) / M;

		for (unsigned int j = 0; j < N; j++)
		{
			float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
			float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

			//Первый треугольник, образующий квад
			vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

			normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

			texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

			//Второй треугольник, образующий квад
			vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
			vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

			normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
			normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

			texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
			texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
		}
	}

	//----------------------------------------

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	leaves = std::make_shared<Mesh>();
	leaves->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	leaves->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	leaves->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	leaves->setPrimitiveType(GL_TRIANGLES);
	leaves->setVertexCount(vertices.size());

	std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

	vertices.clear();
	normals.clear();
	texcoords.clear();

	//front 1
	vertices.push_back(glm::vec3(diag, -diag, 0));
	vertices.push_back(glm::vec3(diag, diag, -height));
	vertices.push_back(glm::vec3(diag, diag, 0));

	normals.push_back(glm::vec3(1.0, 0.0, 0.0));
	normals.push_back(glm::vec3(1.0, 0.0, 0.0));
	normals.push_back(glm::vec3(1.0, 0.0, 0.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));
	texcoords.push_back(glm::vec2(1.0, 1.0));

	//front 2
	vertices.push_back(glm::vec3(diag, -diag, 0));
	vertices.push_back(glm::vec3(diag, -diag, -height));
	vertices.push_back(glm::vec3(diag, diag, -height));

	normals.push_back(glm::vec3(1.0, 0.0, 0.0));
	normals.push_back(glm::vec3(1.0, 0.0, 0.0));
	normals.push_back(glm::vec3(1.0, 0.0, 0.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(0.0, 0.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));

	//left 1
	vertices.push_back(glm::vec3(-diag, -diag, 0));
	vertices.push_back(glm::vec3(diag, -diag, -height));
	vertices.push_back(glm::vec3(diag, -diag, 0));

	normals.push_back(glm::vec3(0.0, -1.0, 0.0));
	normals.push_back(glm::vec3(0.0, -1.0, 0.0));
	normals.push_back(glm::vec3(0.0, -1.0, 0.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));
	texcoords.push_back(glm::vec2(1.0, 1.0));

	//left 2
	vertices.push_back(glm::vec3(-diag, -diag, 0));
	vertices.push_back(glm::vec3(-diag, -diag, -height));
	vertices.push_back(glm::vec3(diag, -diag, -height));

	normals.push_back(glm::vec3(0.0, -1.0, 0.0));
	normals.push_back(glm::vec3(0.0, -1.0, 0.0));
	normals.push_back(glm::vec3(0.0, -1.0, 0.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(0.0, 0.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));

	//back 1
	vertices.push_back(glm::vec3(-diag, -diag, 0));
	vertices.push_back(glm::vec3(-diag, diag, 0));
	vertices.push_back(glm::vec3(-diag, diag, -height));

	normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
	normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
	normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));

	//back 2
	vertices.push_back(glm::vec3(-diag, -diag, 0));
	vertices.push_back(glm::vec3(-diag, diag, -height));
	vertices.push_back(glm::vec3(-diag, -diag, -height));

	normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
	normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
	normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));
	texcoords.push_back(glm::vec2(0.0, 0.0));

	//right 1
	vertices.push_back(glm::vec3(-diag, diag, 0));
	vertices.push_back(glm::vec3(diag, diag, 0));
	vertices.push_back(glm::vec3(diag, diag, -height));

	normals.push_back(glm::vec3(0.0, 1.0, 0.0));
	normals.push_back(glm::vec3(0.0, 1.0, 0.0));
	normals.push_back(glm::vec3(0.0, 1.0, 0.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));

	//right 2
	vertices.push_back(glm::vec3(-diag, diag, 0));
	vertices.push_back(glm::vec3(+diag, diag, -height));
	vertices.push_back(glm::vec3(-diag, diag, -height));

	normals.push_back(glm::vec3(0.0, 1.0, 0.0));
	normals.push_back(glm::vec3(0.0, 1.0, 0.0));
	normals.push_back(glm::vec3(0.0, 1.0, 0.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));
	texcoords.push_back(glm::vec2(0.0, 0.0));

	//bottom 1
	vertices.push_back(glm::vec3(-diag, diag, -height));
	vertices.push_back(glm::vec3(diag, diag, -height));
	vertices.push_back(glm::vec3(diag, -diag, -height));

	normals.push_back(glm::vec3(0.0, 0.0, -1.0));
	normals.push_back(glm::vec3(0.0, 0.0, -1.0));
	normals.push_back(glm::vec3(0.0, 0.0, -1.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));

	//bottom 2
	vertices.push_back(glm::vec3(-diag, diag, -height));
	vertices.push_back(glm::vec3(diag, -diag, -height));
	vertices.push_back(glm::vec3(-diag, -diag, -height));

	normals.push_back(glm::vec3(0.0, 0.0, -1.0));
	normals.push_back(glm::vec3(0.0, 0.0, -1.0));
	normals.push_back(glm::vec3(0.0, 0.0, -1.0));

	texcoords.push_back(glm::vec2(0.0, 1.0));
	texcoords.push_back(glm::vec2(1.0, 0.0));
	texcoords.push_back(glm::vec2(0.0, 0.0));

	//----------------------------------------

	DataBufferPtr buf00 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf00->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf11 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf11->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf22 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf22->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf00);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf11);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf22);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	std::cout << "Cube is created with " << vertices.size() << " vertices\n";

	return mesh;
}

MeshPtr makeCube(float size)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    //front 1
    vertices.push_back(glm::vec3(size, -size, size));
    vertices.push_back(glm::vec3(size, size, -size));
    vertices.push_back(glm::vec3(size, size, size));

    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));

    //front 2
    vertices.push_back(glm::vec3(size, -size, size));
    vertices.push_back(glm::vec3(size, -size, -size));
    vertices.push_back(glm::vec3(size, size, -size));

    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //left 1
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(size, -size, -size));
    vertices.push_back(glm::vec3(size, -size, size));

    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));

    //left 2
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(-size, -size, -size));
    vertices.push_back(glm::vec3(size, -size, -size));

    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //top 1
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(size, -size, size));
    vertices.push_back(glm::vec3(size, size, size));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));

    //top 2
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(size, -size, size));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //back 1
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(-size, size, -size));

    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //back 2
    vertices.push_back(glm::vec3(-size, -size, size));
    vertices.push_back(glm::vec3(-size, size, -size));
    vertices.push_back(glm::vec3(-size, -size, -size));

    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));

    //right 1
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(size, size, size));
    vertices.push_back(glm::vec3(size, size, -size));

    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //right 2
    vertices.push_back(glm::vec3(-size, size, size));
    vertices.push_back(glm::vec3(+size, size, -size));
    vertices.push_back(glm::vec3(-size, size, -size));

    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));

    //bottom 1
    vertices.push_back(glm::vec3(-size, size, -size));
    vertices.push_back(glm::vec3(size, size, -size));
    vertices.push_back(glm::vec3(size, -size, -size));

    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //bottom 2
    vertices.push_back(glm::vec3(-size, size, -size));
    vertices.push_back(glm::vec3(size, -size, -size));
    vertices.push_back(glm::vec3(-size, -size, -size));

    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Cube is created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeScreenAlignedQuad()
{
    std::vector<glm::vec3> vertices;

    //front 1
    vertices.push_back(glm::vec3(-1.0, 1.0, 0.0));
    vertices.push_back(glm::vec3(1.0, -1.0, 0.0));
    vertices.push_back(glm::vec3(1.0, 1.0, 0.0));

    //front 2
    vertices.push_back(glm::vec3(-1.0, 1.0, 0.0));
    vertices.push_back(glm::vec3(-1.0, -1.0, 0.0));
    vertices.push_back(glm::vec3(1.0, -1.0, 0.0));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

MeshPtr makeGroundPlane(float size, float numTiles)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    //front 1
    vertices.push_back(glm::vec3(-size, size, 0.0));
    vertices.push_back(glm::vec3(size, -size, 0.0));
    vertices.push_back(glm::vec3(size, size, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(-numTiles, numTiles));
    texcoords.push_back(glm::vec2(numTiles, -numTiles));
    texcoords.push_back(glm::vec2(numTiles, numTiles));

    //front 2
    vertices.push_back(glm::vec3(-size, size, 0.0));
    vertices.push_back(glm::vec3(-size, -size, 0.0));
    vertices.push_back(glm::vec3(size, -size, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(-numTiles, numTiles));
    texcoords.push_back(glm::vec2(-numTiles, -numTiles));
    texcoords.push_back(glm::vec2(numTiles, -numTiles));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

MeshPtr loadFromAIMesh(const aiMesh &assimpMesh) {
	if (!assimpMesh.HasPositions())
	{
		std::cerr << "This demo does not support meshes without positions\n";
		return std::make_shared<Mesh>();
	}

	if (!assimpMesh.HasNormals())
	{
		std::cerr << "This demo does not support meshes without normals\n";
		return std::make_shared<Mesh>();
	}

	if (!assimpMesh.HasTextureCoords(0))
	{
		std::cerr << "Mesh with no texture coords for texture unit 0 can cause strange visualization\n";
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(assimpMesh.mNumVertices * sizeof(float) * 3, assimpMesh.mVertices);

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(assimpMesh.mNumVertices * sizeof(float) * 3, assimpMesh.mNormals);


	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(assimpMesh.mNumVertices);

	// Upload indices.
	DataBufferPtr indexBuf = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);

	std::vector<unsigned int> indices(3 * assimpMesh.mNumFaces);
	for (int i = 0; i < assimpMesh.mNumFaces; i++) {
		const aiFace &face = assimpMesh.mFaces[i];
		if (face.mNumIndices != 3)
			continue;
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
	}
	indexBuf->setData(indices.size() * sizeof(unsigned int), indices.data());
	mesh->setIndices(indices.size(), indexBuf);

	// Optional upload texcoords0.
	if (assimpMesh.HasTextureCoords(0)) {
		DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		buf2->setData(assimpMesh.mNumVertices * sizeof(float) * 3, assimpMesh.mTextureCoords[0]);
		mesh->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	}

	std::cout << "Mesh " << assimpMesh.mName.data << " is loaded with " << assimpMesh.mNumVertices << " vertices\n";
	return mesh;
}

MeshPtr loadFromFile(const std::string& filename, int meshIndex)
{
    aiEnableVerboseLogging(true);
    auto stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, nullptr);
    aiAttachLogStream(&stream);

    const struct aiScene* assimpScene = aiImportFile(filename.c_str(),
		    aiProcessPreset_TargetRealtime_MaxQuality);

    if (!assimpScene)
    {
        std::cerr << aiGetErrorString() << std::endl;
        return std::make_shared<Mesh>();
    }

    if (assimpScene->mNumMeshes <= meshIndex)
    {
        std::cerr << "Wrong mesh index " << meshIndex << " for file " << filename << std::endl;
        return std::make_shared<Mesh>();
    }

    auto mesh = loadFromAIMesh(*assimpScene->mMeshes[meshIndex]);

    aiReleaseImport(assimpScene);
    aiDetachAllLogStreams();

    return mesh;
}

static float GetHeight(unsigned char point) {
	return static_cast<float>(point) / 25.5;
}

MeshPtr makeTerrain(const glm::mat4& modelMatrix, std::vector< std::vector<glm::vec3> >& heights) {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> mapcoords;
	std::vector<glm::vec2> texcoords;
	
	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setModelMatrix(modelMatrix);
	float size = 10.;
	float numSamples = 100.;
	int width, height, channels;
	unsigned char* image = SOIL_load_image("597ProskurinaData2/hmap.png", &width, &height, &channels, SOIL_LOAD_L);
	if (!image)
	{
		std::cerr << "SOIL loading error: " << SOIL_last_result() << std::endl;
		assert(false);
	}

	float dx = 2 * size / width;
	float dy = 2 * size / height;
	float mdx = 1. / width;
	float mdy = 1. / height;
	float tdx = 1. / ceil(width / numSamples);
	float tdy = 1. / ceil(height / numSamples);

	heights.resize(height);

	for (int y = 0; y < height - 1; ++y) {
		for (int x = 0; x < width - 1; ++x) {
			glm::vec3 current(size - dx * x, -size + dy * y, GetHeight(image[y * width + x]));
			glm::vec3 down(size - dx * x, -size + dy * (y + 1), GetHeight(image[(y + 1) * width + x]));
			glm::vec3 right(size - dx * (x + 1), -size + dy * y, GetHeight(image[y * width + x + 1]));
			glm::vec3 downRight(size - dx * (x + 1), -size + dy * (y + 1), GetHeight(image[(y + 1) * width + x + 1]));

			float useless;
			vertices.push_back(current);
			mapcoords.push_back(glm::vec2(mdx * x, mdy * y));
			texcoords.push_back(glm::vec2(modff(tdx * x, &useless), modff(tdy * y, &useless)));

			vertices.push_back(right);
			mapcoords.push_back(glm::vec2(mdx * (x + 1), mdy * y));
			texcoords.push_back(glm::vec2(modff(tdx * (x + 1), &useless), modff(tdy * y, &useless)));

			vertices.push_back(down);
			mapcoords.push_back(glm::vec2(mdx * x, mdy * (y + 1)));
			texcoords.push_back(glm::vec2(modff(tdx * x, &useless), modff(tdy * (y + 1), &useless)));

			glm::vec3 norm(glm::cross(right - current, down - current));

			if (norm.z < 0) {
				norm *= -1;
			}
			normals.push_back(norm);
			normals.push_back(norm);
			normals.push_back(norm);

			vertices.push_back(downRight);
			mapcoords.push_back(glm::vec2(mdx * (x + 1), mdy * (y + 1)));
			texcoords.push_back(glm::vec2(modff(tdx * (x + 1), &useless), modff(tdy * (y + 1), &useless)));

			vertices.push_back(right);
			mapcoords.push_back(glm::vec2(mdx * (x + 1), mdy * y));
			texcoords.push_back(glm::vec2(modff(tdx * (x + 1), &useless), modff(tdy * y, &useless)));

			vertices.push_back(down);
			mapcoords.push_back(glm::vec2(mdx * x, mdy * (y + 1)));
			texcoords.push_back(glm::vec2(modff(tdx * x, &useless), modff(tdy * (y + 1), &useless)));

			norm = glm::cross(right - downRight, down - downRight);
			if (norm.z < 0) {
				norm *= -1;
			}
			
			normals.push_back(norm);
			normals.push_back(norm);
			normals.push_back(norm);

			heights[y].push_back(modelMatrix * glm::vec4(current, 1.0));
		}
		glm::vec3 current(-size, -size + dy * y, GetHeight(image[y * width + width - 1]));
		heights[y].push_back(modelMatrix * glm::vec4(current, 1.0));
	}
	for (int x = 0; x < width; ++x) {
		glm::vec3 current(size - dx * x, size, GetHeight(image[(height - 1) * width + x]));
		heights[height - 1].push_back(modelMatrix * glm::vec4(current, 1.0));
	}

	SOIL_free_image_data(image);

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf3->setData(mapcoords.size() * sizeof(float) * 2, mapcoords.data());


	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	mesh->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, buf3);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());
	return mesh;
}
