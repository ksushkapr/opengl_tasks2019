add_compile_options(-GLM_ENABLE_EXPERIMENTAL)

set(SRC_FILES
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
		common/Texture.cpp
		common/Framebuffer.cpp
        Terrain.cpp
        )

set(HEADER_FILES
        common/Application.hpp
        common/DebugOutput.h
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
		common/Texture.hpp
		common/framebuffer.hpp
        )

MAKE_OPENGL_TASK(597Proskurina 2 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(597Proskurina2 stdc++fs)
endif ()