#version 330

uniform sampler2D sandTex;
uniform sampler2D grassTex;
uniform sampler2D snowTex;
uniform sampler2D gravelTex;
uniform sampler2D mapTex;
uniform sampler2DShadow shadowTex;

struct LightInfo
{
	vec3 dir; //��������� ��������� ����� � ������� ��������� ����������� ������!
	vec3 La; //���� � ������������� ����������� �����
	vec3 Ld; //���� � ������������� ���������� �����
	vec3 Ls; //���� � ������������� ��������� �����
};
uniform LightInfo light;

in vec3 normalCamSpace; //������� � ������� ��������� ������ (��������������� ����� ��������� ������������)
in vec4 posCamSpace; //���������� ������� � ������� ��������� ������ (��������������� ����� ��������� ������������)
in vec2 texCoord; //���������� ���������� (��������������� ����� ��������� ������������)
in vec2 mapTexCoord;
in vec4 shadowTexCoord;

out vec4 fragColor; //�������� ���� ���������

const mat4x3 Ks = mat4x3( vec3(1.0, 1.0, 1.0),
					vec3(0.1, 0.1, 0.1),
					vec3(0.1, 0.1, 0.1),
					vec3(0.1, 0.1, 0.1) ); //����������� ��������� ���������
const float shininess = 128.0;

vec2 poissonDisk[16] = vec2[]( 
   vec2( -0.94201624, -0.39906216 ), 
   vec2( 0.94558609, -0.76890725 ), 
   vec2( -0.094184101, -0.92938870 ), 
   vec2( 0.34495938, 0.29387760 ), 
   vec2( -0.91588581, 0.45771432 ), 
   vec2( -0.81544232, -0.87912464 ), 
   vec2( -0.38277543, 0.27676845 ), 
   vec2( 0.97484398, 0.75648379 ), 
   vec2( 0.44323325, -0.97511554 ), 
   vec2( 0.53742981, -0.47373420 ), 
   vec2( -0.26496911, -0.41893023 ), 
   vec2( 0.79197514, 0.19090188 ), 
   vec2( -0.24188840, 0.99706507 ), 
   vec2( -0.81409955, 0.91437590 ), 
   vec2( 0.19984126, 0.78641367 ), 
   vec2( 0.14383161, -0.14100790 ) 
);

const float radiusInv = 700.0;

layout(location = 0) out vec4 colorTex; // "go to GL_COLOR_ATTACHMENT1"

void main()
{
	vec4 shadowCoords = shadowTexCoord;		
	shadowCoords.xyz /= shadowCoords.w;
    //shadowCoords.z += bias;

	float visibility = 1.0;
	for (int i = 0; i < 4; i++)
	{		
		visibility -= 0.25 * (1.0 - texture(shadowTex, vec3(shadowCoords.xy + poissonDisk[i] / radiusInv, shadowCoords.z)));
	}
	
	//===============================

	vec3 sandColor = texture(sandTex, texCoord).rgb;
	vec3 snowColor = texture(snowTex, texCoord).rgb;
	vec3 grassColor = texture(grassTex, texCoord).rgb;
	vec3 gravelColor = texture(gravelTex, texCoord).rgb;
	vec4 mapColor = texture(mapTex, mapTexCoord).rgba;
	mapColor.a = 1.0 - mapColor.a;

	//vec3 diffuseColor = mapColor.rgb;
	vec3 diffuseColor = grassColor * mapColor.g + snowColor * mapColor.r + gravelColor * mapColor.b + sandColor * mapColor.a;
	
	vec3 KsVec = Ks * mapColor;
	
	vec3 lightDirCamSpace = normalize(light.dir); //����������� �� �������� �����

	vec3 normal = normalize(normalCamSpace); //����������� ������� ����� ������������
	
	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //��������� ������������ (�������)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

	if (NdotL > 0.0)
	{			
		vec3 viewDirection = normalize(-posCamSpace.xyz); //����������� �� ����������� ������ (��� ��������� � ����� (0.0, 0.0, 0.0))
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //����������� ����� ������������� �� ������ � �� �������� �����

		float blinnTerm = max(dot(normal, halfVector), 0.0); //������������� ��������� ��������� �� ������				
		blinnTerm = pow(blinnTerm, shininess); //���������� ������ �����
		
		color += light.Ls * KsVec * blinnTerm;
	}
	colorTex = vec4(color, 1.);
	fragColor = vec4(0.,0.,0., 1.);


	/*vec4 shadowCoords = shadowTexCoord;		
	shadowCoords.xyz /= shadowCoords.w;
	float r = texture(shadowTex, shadowCoords.rgb);
	colorTex = vec4(r, 0,0, 1.);*/
}
