#version 420

uniform sampler2D tex; //основная текстура

in vec2 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

void main()
{	
	vec3 color = texture(tex, texCoord).rgb;

	//Вариант 1 (с экспозицией)
	float exposure = exp(0.2);
	fragColor.rgb = vec3(1.0) - exp(-color*exposure); //tone mapping: HDR to LDR
	
	//Вариант 2 (Reinhard tone mapping)
	//fragColor.rgb = color / (color + vec3(1.0));


	//fragColor = vec4(color, 1.0);
	fragColor.a = 1.0;
}
