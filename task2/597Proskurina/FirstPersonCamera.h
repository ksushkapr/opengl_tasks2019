﻿#pragma once

#include "common/Camera.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

class FirstPersonCamera : public FreeCameraMover {
public:
	void SetHeights(std::vector< std::vector< glm::vec3> >* _heights) { heights = _heights; }
	bool isFree = true;

	void update(GLFWwindow* window, double dt) override
	{
		float speed = 1.0f;

		glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

		glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		{
			_pos += forwDir * speed * static_cast<float>(dt);
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		{
			_pos -= forwDir * speed * static_cast<float>(dt);
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		{
			_pos -= rightDir * speed * static_cast<float>(dt);
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		{
			_pos += rightDir * speed * static_cast<float>(dt);
		}

		if (!isFree) {
			findNearestPoint();
			_pos.z = getHeight() + 0.3;
		}
		//-----------------------------------------

		_camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

		//-----------------------------------------

		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		_camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
	}

private:
	std::vector< std::vector< glm::vec3> >* heights;
	int lastX = 0;
	int lastY = 0;

	enum CameraMode {
		FirstPerson,
		Free,
		Count
	} mode = Free;

	void findGlobalNearestPoint() {
		float maxDist = INFINITY;
		glm::vec2 pos(_pos.x, _pos.y);
		for (int i = 0; i < heights->size(); ++i) {
			for (int j = 0; j < (*heights)[i].size(); ++j) {
				float dist = glm::distance2(glm::vec2((*heights)[i][j].x, (*heights)[i][j].y), pos);
				if (dist < maxDist) {
					maxDist = dist;
					lastX = i;
					lastY = j;
				}
			}
		}
	}

	void findNearestPoint() {
		float maxDist = INFINITY;
		glm::vec2 pos(_pos.x, _pos.y);
		int newLastX = lastX;
		int newLastY = lastY;
		for (int i = -15; i <= 15; ++i) {
			if (lastX + i < 0 || lastX + i >= heights->size())
				continue;
			for (int j = -15; j <= 15; ++j) {
				if (lastY + j < 0 || lastY + j >= (*heights)[lastX + i].size())
					continue;
				float dist = glm::distance2(glm::vec2((*heights)[lastX + i][lastY + j].x, (*heights)[lastX + i][lastY + j].y), pos);
				if (dist < maxDist) {
					maxDist = dist;
					newLastX = lastX + i;
					newLastY = lastY + j;
				}
			}
		}
		lastX = newLastX;
		lastY = newLastY;
		if (maxDist > 5.) {
			findGlobalNearestPoint();
		}
	}

	float getHeight() {
		float result = 0;
		glm::vec2 pos(_pos.x, _pos.y);
		float sumDist = 0;
		for (int i = -1; i <= 1; ++i) {
			for (int j = -1; j <= 1; ++j) {
				if (lastX + i < 0 || lastX + i >= heights->size())
					continue;
				if (lastY + j < 0 || lastY + j >= (*heights)[lastX + i].size())
					continue;
				float dist = 1. / std::fmax(glm::distance2(glm::vec2((*heights)[lastX + i][lastY + j].x, (*heights)[lastX + i][lastY + j].y), pos), 0.0001);
				result += dist * (*heights)[lastX + i][lastY + j].z;
				sumDist += dist;
			}
		}
		return (sumDist != 0) ? result / sumDist : 0;
	}
};