#include "common/Application.hpp"
#include "common/ShaderProgram.hpp"
#include "common/LightInfo.hpp"
#include "common/Camera.hpp"
#include "common/Mesh.hpp"
#include "common/Texture.hpp"
#include "common/Framebuffer.hpp"

#include <iostream>
#include <vector>
#include "common/Common.h"
#include "FirstPersonCamera.h"

/**
����������� � ������������������ �������
*/

float frand()
{
	return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

class TerrainApplication : public Application
{
public:
	MeshPtr mesh;
	MeshPtr treeBot;
	MeshPtr treeTop;

	MeshPtr _marker; //��� - ������ ��� ��������� �����
	MeshPtr _marker2;

	//������������� ��������� ���������
	ShaderProgramPtr _commonShader;
	ShaderProgramPtr _quadColorShader;
	ShaderProgramPtr _toneMappingShader;
	ShaderProgramPtr _skyboxShader;
	ShaderProgramPtr _treeShader;
	ShaderProgramPtr _renderToShadowMapShader;
	ShaderProgramPtr _renderInstancedToShadowMapShader;

	//���������� ��� ���������� ���������� ������ ��������� �����
	float _lr = 10.0;
	float _phi = 4.0f;
	float _theta = 0.48f;

	DirectLightInfo _light;
	glm::vec3 lightBasePos{ 0., 0., 2. };
	float _diffuseCoef = 1.;
	float _ambientCoef = 1.;
	float _specularCoef = 1.;
	CameraInfo _lightCameraInfo;
	std::shared_ptr<OrbitCameraMover> _lightCamera;

	TexturePtr _sandTex;
	TexturePtr _gravelTex;
	TexturePtr _snowTex;
	TexturePtr _grassTex;
	TexturePtr _mapTex;
	TexturePtr _leavesTex;
	TexturePtr _treeTex;

	GLuint _sandSampler;
	GLuint _gravelSampler;
	GLuint _snowSampler;
	GLuint _grassSampler;
	GLuint _mapSampler;
	GLuint _depthSamplerLinear;

	std::vector< std::vector<glm::vec3> > heights;

	GLuint _sampler;

	FramebufferPtr _gbufferFB;
	TexturePtr _depthTex;
	TexturePtr _diffuseTex;

	int _oldWidth = 1024;
	int _oldHeight = 1024;

	MeshPtr _quad;

	GLuint _frameBrigthnessBO;
	GLuint _frameBrigthnessResultBO;
	std::vector<float> _frameBrigthness;
	float _frameBrigthnessResult;

	FramebufferPtr _toneMappingFB;
	TexturePtr _toneMappingTex;

	FramebufferPtr _shadowFB;
	TexturePtr _shadowTex;

	MeshPtr _backgroundCube;
	TexturePtr _cubeTex;
	GLuint _cubeTexSampler;
	float _backCoef = 5.;
	float _adaptation = 0.1;

	std::vector<glm::vec4> _positions;
	DataBufferPtr _bufVec4;
	unsigned int instanceCount = 100;
	float treeHeight = 1.f;

	TerrainApplication() :
		Application()
	{
		_cameraMover = std::make_shared<FirstPersonCamera>();
	}

	GLuint _framebufferId;
	GLuint _depthTexId;

	void initFramebuffers()
	{
		//������� ���������� ��� ���������� � G-�����
		_gbufferFB = std::make_shared<Framebuffer>(1024, 1024);

		_diffuseTex = _gbufferFB->addBuffer(GL_RGBA16F, GL_COLOR_ATTACHMENT0);
		_depthTex = _gbufferFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

		_gbufferFB->initDrawBuffers();

		if (!_gbufferFB->valid())
		{
			std::cerr << "Failed to setup framebuffer\n";
			assert(false);
		}

		_shadowFB = std::make_shared<Framebuffer>(1024, 1024);

		_shadowTex = _shadowFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

		_shadowFB->initDrawBuffers();

		if (!_shadowFB->valid())
		{
			std::cerr << "Failed to setup framebuffer\n";
			assert(false);
		}

		_toneMappingFB = std::make_shared<Framebuffer>(1024, 1024);

		_toneMappingTex = _toneMappingFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT0);

		_toneMappingFB->initDrawBuffers();

		if (!_toneMappingFB->valid())
		{
			std::cerr << "Failed to setup framebuffer\n";
			assert(false);
		}

		for (unsigned int i = 0; i < instanceCount; i++)
		{
			//_positionsVec3.push_back(glm::vec3(frand() * size - 0.5 * size, frand() * size - 0.5 * size, 0.0));
			int x = rand() % heights.size();
			int y = rand() % heights[x].size();

			_positions.push_back(glm::vec4(heights[x][y], 0.));
			_positions.back().z += treeHeight;
		}

		_bufVec4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		_bufVec4->setData(_positions.size() * sizeof(float) * 4, _positions.data());

		//----------------------------

		//����������� SSBO � 0� ����� �������� (��������� ����� � �������������)
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, _bufVec4->id());




		glGenFramebuffers(1, &_framebufferId);
		glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

		//----------------------------

		//������� ��������, ���� ����� ������������ ���������� ����� �������
		glGenTextures(1, &_depthTexId);
		glBindTexture(GL_TEXTURE_2D, _depthTexId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthTexId, 0);

		//----------------------------

		// ���������, ��� ��� �������� ����������� ������ ����� ������������ ������� ������ �� ������.
		GLenum buffers[] = { GL_NONE };
		glDrawBuffers(1, buffers);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			std::cerr << "Failed to setup framebuffer\n";
			exit(1);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	};

	void makeScene() override
	{
		Application::makeScene();
		mesh = makeTerrain(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, -3.5f)), heights);
		dynamic_cast<FirstPersonCamera*>(_cameraMover.get())->SetHeights(&heights);
		_lightCamera = std::make_shared<OrbitCameraMover>();

		treeBot = makeTree(treeHeight, treeTop);

		_quad = makeScreenAlignedQuad();

		_backgroundCube = makeCube(15.0f);

		_commonShader = std::make_shared<ShaderProgram>("597ProskurinaData2/shaders/common.vert", "597ProskurinaData2/shaders/common.frag");
		_quadColorShader = std::make_shared<ShaderProgram>("597ProskurinaData2/shaders/quadColor.vert", "597ProskurinaData2/shaders/quadColor.frag");
		_toneMappingShader = std::make_shared<ShaderProgram>("597ProskurinaData2/shaders/quadColor.vert", "597ProskurinaData2/shaders/tonemapping.frag");
		_skyboxShader = std::make_shared<ShaderProgram>("597ProskurinaData2/shaders/skybox.vert", "597ProskurinaData2/shaders/skybox.frag");
		_treeShader = std::make_shared<ShaderProgram>("597ProskurinaData2/shaders/common_tree.vert", "597ProskurinaData2/shaders/common_tree.frag");
		_renderToShadowMapShader = std::make_shared<ShaderProgram>("597ProskurinaData2/shaders/toshadow.vert", "597ProskurinaData2/shaders/toshadow.frag");
		_renderInstancedToShadowMapShader = std::make_shared<ShaderProgram>("597ProskurinaData2/shaders/toshadow_tree.vert", "597ProskurinaData2/shaders/toshadow.frag");

		_sandTex = loadTexture("597ProskurinaData2/sand.jpg");
		_gravelTex = loadTexture("597ProskurinaData2/rock.jpg");
		_snowTex = loadTexture("597ProskurinaData2/snow.jpg");
		_grassTex = loadTexture("597ProskurinaData2/grass.jpg");
		_mapTex = loadTexture("597ProskurinaData2/tmap.png");
		_cubeTex = loadCubeTexture("597ProskurinaData2/cube");
		_leavesTex = loadTexture("597ProskurinaData2/leaves.jpg");
		_treeTex = loadTexture("597ProskurinaData2/tree_bot.jpg");

		//=========================================================
		//������������� �������� ���������� ��������
		_light.direction = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_light.ambient = glm::vec3(0.6, 0.6, 0.6);
		_light.diffuse = glm::vec3(0.4, 0.4, 0.4);
		_light.specular = glm::vec3(1.0, 1.0, 1.0);

		//=========================================================
		//������������� ��������, �������, ������� ������ ��������� ������ �� ��������
		glGenSamplers(1, &_sandSampler);
		glSamplerParameteri(_sandSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_sandSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_sandSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sandSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_gravelSampler);
		glSamplerParameteri(_gravelSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_gravelSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_gravelSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_gravelSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_snowSampler);
		glSamplerParameteri(_snowSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_snowSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_snowSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_snowSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_grassSampler);
		glSamplerParameteri(_grassSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_grassSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_grassSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_grassSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_mapSampler);
		glSamplerParameteri(_mapSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_mapSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_mapSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_mapSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glGenSamplers(1, &_cubeTexSampler);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		GLfloat border[] = { 1.0f, 0.0f, 0.0f, 1.0f };

		glGenSamplers(1, &_depthSamplerLinear);
		glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glSamplerParameterfv(_depthSamplerLinear, GL_TEXTURE_BORDER_COLOR, border);
		glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

		initFramebuffers();
	}

	void update()
	{
		Application::update();
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);
		if (width != _oldWidth || height != _oldHeight)
		{
			_gbufferFB->resize(width, height);
			_toneMappingFB->resize(width, height);
			_shadowFB->resize(width, height);
			_oldWidth = width;
			_oldHeight = height;
		}


		_light.direction = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_lightCamera->setOrientationParameters(_lr, _phi, _theta);
		_lightCameraInfo = _lightCamera->cameraInfo();
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			ImGui::Checkbox("FreeCam", &dynamic_cast<FirstPersonCamera*>(_cameraMover.get())->isFree);
			ImGui::SliderFloat("background", &_backCoef, 0., 10.);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::SliderFloat("ambientCoef", &_ambientCoef, 0., 10.);
				ImGui::SliderFloat("diffuseCoef", &_diffuseCoef, 0., 10.);
				ImGui::SliderFloat("specularCoef", &_specularCoef, 0., 10.);

				ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
				ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
				ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}
		}
		ImGui::End();
	}

	void draw() override
	{
		//�������� ��������� ����� � G-�����
		//drawToShadowBuffer(_shadowFB);
		drawToGBuffer(_gbufferFB, _commonShader, _camera);

		drawToneMapping(_toneMappingFB, _toneMappingShader);
		drawToScreen(_quadColorShader, _toneMappingTex);

	}

	void drawScene(const ShaderProgramPtr& shader, const CameraInfo& camera) {

		shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

		_light.direction = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		glm::vec3 lightDirectionCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(lightBasePos + _light.direction, 1.0)) -
			glm::vec3(_camera.viewMatrix * glm::vec4(lightBasePos, 1.0));

		shader->setVec3Uniform("light.dir", lightDirectionCamSpace); //�������� ��������� ��� � ������� ����������� ������
		shader->setVec3Uniform("light.La", _light.ambient * _ambientCoef);
		shader->setVec3Uniform("light.Ld", _light.diffuse * _diffuseCoef);
		shader->setVec3Uniform("light.Ls", _light.specular * _specularCoef);

		glActiveTexture(GL_TEXTURE0);  //���������� ���� 0        
		glBindSampler(0, _sandSampler);
		_sandTex->bind();
		shader->setIntUniform("sandTex", 0);

		glActiveTexture(GL_TEXTURE1);  //���������� ���� 1        
		glBindSampler(1, _snowSampler);
		_snowTex->bind();
		shader->setIntUniform("snowTex", 1);

		glActiveTexture(GL_TEXTURE2);  //���������� ���� 2        
		glBindSampler(2, _grassSampler);
		_grassTex->bind();
		shader->setIntUniform("grassTex", 2);

		glActiveTexture(GL_TEXTURE3);  //���������� ���� 3        
		glBindSampler(3, _gravelSampler);
		_gravelTex->bind();
		shader->setIntUniform("gravelTex", 3);

		glActiveTexture(GL_TEXTURE4);  //���������� ���� 4        
		glBindSampler(4, _mapSampler);
		_mapTex->bind();
		shader->setIntUniform("mapTex", 4);

		glActiveTexture(GL_TEXTURE5);  //���������� ���� 0        
		glBindSampler(5, _depthSamplerLinear);
		_shadowTex->bind();
		shader->setIntUniform("shadowTex", 5);


		//��������� �� ���������� ������� ������ ����� � ��������� ���������
		{
			shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
			shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));
			shader->use();
			mesh->draw();
		}

		glBindSampler(0, 0);
		glBindSampler(1, 0);
		glBindSampler(2, 0);
		glBindSampler(3, 0);
		glBindSampler(4, 0);
	}

	void drawShadowScene(const ShaderProgramPtr& shader, const CameraInfo& camera) {

		//��������� �� ���������� ������� ������ ����� � ��������� ���������
		{
			shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
			//shader->use();
			mesh->draw();
		}
	}

	void drawSkybox(const ShaderProgramPtr& shader) {
		{

			glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]); //��������� �� ������� ���� ��������� ����������� ������ � ������� ������� ���������

			shader->setVec3Uniform("cameraPos", cameraPos);
			shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
			shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
			shader->setFloatUniform("coef", _backCoef);

			//��� �������������� ��������� � ���������� ���������� ����� ����������� �������
			glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
			shader->setMat3Uniform("textureMatrix", textureMatrix);

			glActiveTexture(GL_TEXTURE0);  //���������� ���� 0            
			glBindSampler(0, _cubeTexSampler);
			_cubeTex->bind();
			shader->setIntUniform("cubeTex", 0);

			glDepthMask(GL_FALSE); //��������� ������ � ����� �������

			_backgroundCube->draw();

			glDepthMask(GL_TRUE); //�������� ������� ������ � ����� �������
		}
	}

	void drawToShadowBuffer(const FramebufferPtr& fb)
	{
		fb->bind();
		//glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);
		GLenum buffers[] = { GL_NONE };
		glDrawBuffers(1, buffers);
		glViewport(0, 0, fb->width(), fb->height());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		

		//_light.direction = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		//_lightCamera.viewMatrix = glm::lookAt(glm::vec3(0.0f, -1.0f, -3.3f), glm::vec3(0.,0.,-4.), glm::vec3(0.0f, 0.0f, 1.0f));
		//_lightCamera.projMatrix = glm::perspective(glm::radians(45.0f), 1.0f, 0.1f, 100.f);

		_renderToShadowMapShader->use();
		_renderToShadowMapShader->setMat4Uniform("lightViewMatrix", _lightCameraInfo.viewMatrix);
		_renderToShadowMapShader->setMat4Uniform("lightProjectionMatrix", _lightCameraInfo.projMatrix);

		drawShadowScene(_renderToShadowMapShader, _camera);

		/*(_renderInstancedToShadowMapShader->use();
		_renderInstancedToShadowMapShader->setMat4Uniform("lightViewMatrix", _lightCamera.viewMatrix);
		_renderInstancedToShadowMapShader->setMat4Uniform("lightProjectionMatrix", _lightCamera.projMatrix);
		drawTrees(_renderInstancedToShadowMapShader);
		*/
		glUseProgram(0); //��������� ������
		//glBindFramebuffer(GL_FRAMEBUFFER, 0); //��������� ����������
		fb->unbind(); //��������� ����������
	}

	void drawToGBuffer(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera)
	{
		fb->bind();

		glViewport(0, 0, fb->width(), fb->height());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader->use();

		{
			shader->setMat4Uniform("lightViewMatrix", _lightCameraInfo.viewMatrix);
			shader->setMat4Uniform("lightProjectionMatrix", _lightCameraInfo.projMatrix);

			glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
			shader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);
		}

		drawScene(shader, camera);
		_skyboxShader->use();
		drawSkybox(_skyboxShader);
		_treeShader->use();

		{
			_treeShader->setMat4Uniform("lightViewMatrix", _lightCameraInfo.viewMatrix);
			_treeShader->setMat4Uniform("lightProjectionMatrix", _lightCameraInfo.projMatrix);

			glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
			_treeShader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);
		}
		drawTrees(_treeShader);

		glUseProgram(0); //��������� ������

		fb->unbind(); //��������� ����������
	}

	void drawTrees(const ShaderProgramPtr& shader) {


		_light.direction = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		glm::vec3 lightDirectionCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(lightBasePos + _light.direction, 1.0)) -
			glm::vec3(_camera.viewMatrix * glm::vec4(lightBasePos, 1.0));

		shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		shader->setVec3Uniform("light.dir", lightDirectionCamSpace); //�������� ��������� ��� � ������� ����������� ������
		shader->setVec3Uniform("light.La", _light.ambient * _ambientCoef);
		shader->setVec3Uniform("light.Ld", _light.diffuse * _diffuseCoef);
		shader->setVec3Uniform("light.Ls", _light.specular * _specularCoef);


		unsigned int ssboIndex = glGetProgramResourceIndex(shader->id(), GL_SHADER_STORAGE_BLOCK, "Positions");
		glShaderStorageBlockBinding(shader->id(), ssboIndex, 0);

		//��������� �� ���������� ������� ������ ����� � ��������� ���������
		{
			shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
			shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));
			shader->use();

			glActiveTexture(GL_TEXTURE1);  //���������� ���� 0        
			glBindSampler(1, _sampler);
			_shadowTex->bind();
			shader->setIntUniform("shadowTex", 1);

			glActiveTexture(GL_TEXTURE0);  //���������� ���� 0        
			glBindSampler(0, _sampler);
			_treeTex->bind();
			shader->setIntUniform("tex", 0);

			treeBot->drawInstanced(instanceCount);

			glActiveTexture(GL_TEXTURE0);  //���������� ���� 0        
			glBindSampler(0, _sampler);
			_leavesTex->bind();
			shader->setIntUniform("tex", 0);

			treeTop->drawInstanced(instanceCount);
			//treeBot->draw();
			//treeTop->draw();
		}

		glBindSampler(0, 0);
	}

	void drawToneMapping(const FramebufferPtr& fb, const ShaderProgramPtr& shader)
	{
		fb->bind();

		glViewport(0, 0, fb->width(), fb->height());
		glClear(GL_COLOR_BUFFER_BIT);

		shader->use();

		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _sampler);
		_diffuseTex->bind();
		shader->setIntUniform("tex", 0);

		_quad->draw();

		glUseProgram(0);

		fb->unbind();
	}

	void drawToScreen(const ShaderProgramPtr& shader, const TexturePtr& inputTexture)
	{
		//�������� ������� ������� ������ � ��������� �������
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader->use();

		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _sampler);
		inputTexture->bind();
		//glBindTexture(GL_TEXTURE_2D, _depthTexId);
		shader->setIntUniform("tex", 0);

		glEnable(GL_FRAMEBUFFER_SRGB); //�������� �����-���������

		_quad->draw();

		glDisable(GL_FRAMEBUFFER_SRGB);

		//����������� ������� � ��������� ���������
		glBindSampler(0, 0);
		glUseProgram(0);
	}

};

int main()
{
	TerrainApplication app;
	app.start();

	return 0;
}